from transforms import *
from images import read_img, write_img
import sys

def main():
    if len(sys.argv) != 3: #verifica si el numero de argumentos es 3
        print("USO: python transforms_simple.py <input_image> <transform>") #la forma en lo que lo tienes que poner
        return

    input_filename = sys.argv[1] #fichero de entrada (cafe.jpg)
    transform = sys.argv[2] #la transformacion se aplica al argumento 2

    image = read_img(input_filename)

    if transform == 'mirror':
        transformed_image = mirror(image) #la imagen leida(Cafe.jpg) se cambia a la transformacion mirror
    elif transform == 'grayscale':
        transformed_image = grayscale(image) #la imagen leida(Cafe.jpg) se cambia a la transformacion grayscale
    elif transform == 'blur':
        transformed_image = blur(image) #la imagen leida(Cafe.jpg) se cambia a la transformacion blur
    else:
        print(f"TRANSFORMACION", transform, "NO RECONOCIDA.")
        return

    contador = input_filename.split('/')[-1].split('.')[0]
    write_img(transformed_image,f"/home/alumnos/lidia/materiales/TRABAJOFINAL/{contador}_trans.jpg")

if __name__ == '__main__':
    main()