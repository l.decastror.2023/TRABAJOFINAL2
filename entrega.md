#ENTREGA CONVOCATORIA JUNIO 
NOMBRE: Lidia de Castro Rodríguez
CORREO: l.decastror.2023@alumnos.urjc.es
ENLACE AL VIDEO: https://youtu.be/N_2jEcm7xPc 

REQUISITOS MÍNIMOS:
Módulo transforms.py:
- Método mirror 
- Método grayscale
- Método blur 
- Método change_colors
- Método shift
- Método crop
- Método filter

Módulo transform_simple.py
Módulo transform_args.py
Módulo transform_multi.py
